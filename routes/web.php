<?php

Route::get('/', function () {
    return view('welcome');
});

// To run locally:
// php artisan serve --host 0.0.0.0
Route::get('/taxis', 'TaxiController@index');
Route::get('/taxis/{taxi}/status', 'TaxiController@status');
