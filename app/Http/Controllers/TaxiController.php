<?php

namespace App\Http\Controllers;

use App\Taxi;
use Illuminate\Http\Request;

class TaxiController extends Controller
{
    public function index()
    {
        $data = [];

        $taxis = Taxi::with([
            'propietario' => function ($query) {
                $query->select('NUMERO_CLIENTE', 'NOMBRE');
            }
        ])->get([
            'NUMERO_TAXI',
            'NUMERO_PROPIETARIO',
            'ESTATUS'
        ])->map(function ($taxi) {
            $name = $taxi->propietario->NOMBRE;
            unset($taxi->propietario);
            $taxi->PROPIETARIO = $name;
            return $taxi;
        });

        $data['taxis'] = $taxis;
        return $data;
    }

    public function status(Request $request, Taxi $taxi)
    {
        $newStatus = $request->input('status');

        if ($newStatus == 'A' || $newStatus == 'I') {
            $taxi->ESTATUS = $newStatus;
            $saved = $taxi->save();
        } else {
            $saved = false;
        }

        $data = [];
        $data['success'] = $saved;
        return $data;
    }

}
