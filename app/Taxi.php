<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxi extends Model
{
    protected $table = 'taxis';
    protected $primaryKey = 'NUMERO_TAXI';
    public $timestamps = null;
    public $incrementing = false;

    public function propietario()
    {
        return $this->belongsTo(Propietario::class, 'NUMERO_PROPIETARIO', 'NUMERO_CLIENTE');
    }
}
